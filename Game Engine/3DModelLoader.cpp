#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <d3d11.h>
#include "3DModelLoader.h"

#define BUFSIZE 65536

ModelLoader::ModelLoader() {
	vertex = nullptr;
	vertexCount = 0;
}

ModelLoader::~ModelLoader() {
	if (vertex != NULL)
		free(vertex);

	vertex = NULL;
	vertexCount = 0;
}

bool GetToken(FILE* fp, char* buf) {
	buf[0] = 0;
	return (fscanf_s(fp, "%s", buf, BUFSIZE) != EOF);
}

bool SkipLine(FILE* fp) {
	char ch;

	do {
		ch = fgetc(fp);
	} while ((ch != 10) && (ch != 13) && (ch != EOF));

	return (ch != EOF);
}

bool AddFloatBuffer(float value, float** ppBuf, unsigned long* pBufSize, unsigned long* pBufPos) {
	if ((*ppBuf) == NULL)
		return false;

	(*ppBuf)[*pBufPos] = value;
	(*pBufPos)++;

	if ((*pBufPos) >= (*pBufSize)) {
		(*ppBuf) = (float*)realloc((void*)(*ppBuf), ((*pBufSize) + BUFSIZE) * sizeof(float));
		if ((*ppBuf) == NULL) { return false; }

		(*pBufSize) += BUFSIZE;
	}

	return true;
}

bool AddIntBuffer(int value, int** ppBuf, unsigned long* pBufSize, unsigned long* pBufPos) {
	if ((*ppBuf) == NULL)
		return false;

	(*ppBuf)[*pBufPos] = value;
	(*pBufPos)++;

	if ((*pBufPos) >= (*pBufSize)) {
		(*ppBuf) = (int*)realloc((void*)(*ppBuf), ((*pBufSize) + BUFSIZE) * sizeof(int));

		if ((*ppBuf) == NULL)
			return false;

		(*pBufSize) += BUFSIZE;
	}

	return true;
}

bool ModelLoader::LoadFile(char* szFileName) {
	// Open file
	FILE* fp = NULL;
	fopen_s(&fp, szFileName, "rb");

	// File wasn't opened.
	if (fp == NULL)
		return false;

	int r, a, b, c, i;
	char buf[BUFSIZE];

	// File vertexes.
	float* vbuf = (float*)malloc(BUFSIZE * sizeof(float));
	unsigned long vbufsize = BUFSIZE;
	unsigned long vbufpos = 0;

	// File vertex normals.
	float* vnbuf = (float*)malloc(BUFSIZE * sizeof(float));
	unsigned long vnbufsize = BUFSIZE;
	unsigned long vnbufpos = 0;

	// File vertex texture coordinates.
	float* vtbuf = (float*)malloc(BUFSIZE * sizeof(float));
	unsigned long vtbufsize = BUFSIZE;
	unsigned long vtbufpos = 0;

	// File faces.
	int* fbuf = (int*)malloc(BUFSIZE * sizeof(int));
	unsigned long fbufsize = BUFSIZE;
	unsigned long fbufpos = 0;

	// Check buffers.
	if ((vbuf == NULL) || (vnbuf == NULL) || (vtbuf == NULL) || (fbuf == NULL))
		return false; // out of memory

	while (!feof(fp)) {
		// Get token.
		r = GetToken(fp, buf);
		if (!r) { break; }

		// Vertexes.
		if (strcmp(buf, "v") == 0) {
			GetToken(fp, buf);
			AddFloatBuffer((float)atof(buf), &vbuf, &vbufsize, &vbufpos);
			GetToken(fp, buf);
			AddFloatBuffer((float)atof(buf), &vbuf, &vbufsize, &vbufpos);
			GetToken(fp, buf);
			AddFloatBuffer((float)atof(buf), &vbuf, &vbufsize, &vbufpos);
			continue;
		}

		// Vertex normals.
		if (strcmp(buf, "vn") == 0) {
			GetToken(fp, buf);
			AddFloatBuffer((float)atof(buf), &vnbuf, &vnbufsize, &vnbufpos);
			GetToken(fp, buf);
			AddFloatBuffer((float)atof(buf), &vnbuf, &vnbufsize, &vnbufpos);
			GetToken(fp, buf);
			AddFloatBuffer((float)atof(buf), &vnbuf, &vnbufsize, &vnbufpos);
			continue;
		}

		// Vertex texture coordinates.
		if (strcmp(buf, "vt") == 0) {
			GetToken(fp, buf);
			AddFloatBuffer((float)atof(buf), &vtbuf, &vtbufsize, &vtbufpos);
			GetToken(fp, buf);
			AddFloatBuffer((float)atof(buf), &vtbuf, &vtbufsize, &vtbufpos);
			continue;
		}

		// Vertex faces.
		if (strcmp(buf, "f") == 0) {
			for (i = 0; i < 3; i++) {
				GetToken(fp, buf);
				a = b = c = 0;

				if (strstr(buf, "//") == 0)
					sscanf_s(buf, "%d/%d/%d", &a, &b, &c, BUFSIZE);

				else
					sscanf_s(buf, "%d//%d", &a, &c, BUFSIZE);

				AddIntBuffer(a, &fbuf, &fbufsize, &fbufpos);
				AddIntBuffer(b, &fbuf, &fbufsize, &fbufpos);
				AddIntBuffer(c, &fbuf, &fbufsize, &fbufpos);
			}

			continue;
		}

		// Skip comments and unknown items.
		SkipLine(fp);
	}

	vertexCount = fbufpos / 3;
	vertex = (Vertex::Basic32*)malloc(vertexCount * sizeof(Vertex::Basic32));
	if (vertex == NULL)
		return false;

	::memset((void*)vertex, 0, vertexCount * sizeof(Vertex::Basic32));
	unsigned long vi, vti, vni, f;
	for (i = 0; i < (int)vertexCount; i++) {
		f = i * 3;
		vi = fbuf[f + 0] - 1;
		vti = fbuf[f + 1] - 1;
		vni = fbuf[f + 2] - 1;

		// vertex
		if (vi < vbufpos) {
			vertex[i].Pos.x = vbuf[vi * 3 + 0];
			vertex[i].Pos.y = vbuf[vi * 3 + 1];
			vertex[i].Pos.z = vbuf[vi * 3 + 2];
		}

		// texture coordinate
		if (vti < vtbufpos) {
			vertex[i].Tex.x = vtbuf[vti * 2 + 0];
			vertex[i].Tex.y = vtbuf[vti * 2 + 1];
		}

		// normal
		if (vni < vnbufpos) {
			vertex[i].Normal.x = vnbuf[vni * 3 + 0];
			vertex[i].Normal.y = vnbuf[vni * 3 + 1];
			vertex[i].Normal.z = vnbuf[vni * 3 + 2];
		}
	}


	// Cleanup
	free(vbuf);
	free(vnbuf);
	free(vtbuf);
	free(fbuf);
	fclose(fp);

	return true;
}

// Clears the loaded data
void ModelLoader::Clear() {
	if (vertex != NULL)
		free(vertex);

	vertex = NULL;
	vertexCount = 0;
}