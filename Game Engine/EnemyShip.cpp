#include "Camera.h"
#include "EnemyShip.h"

EnemyShip::EnemyShip(float mX, float mY, float mZ, bool iPredator) : mEnemyShipDiffuseMapSRV(0), mADetect(5.0f), mMDetect(10.0f) {

	mIsPredator = iPredator;

	// Predators are larger than regular EnemyShips. In these statements you can change the size of the cones (wouldn't recommend it)
	// and you can change their maximum speed.
	if (mIsPredator) {
		mBase = 1.0f;
		mHeight = 2.0f;
		mMaxSpeed = 0.05f; // 0.03
		mMaxForce = 0.005f;

		// A predator has a larger centering range.
		mCDetect = 30.0f;
	}

	else {
		mBase = 0.5f;
		mHeight = 1.0f;
		mMaxSpeed = 0.05f; // 0.03
		mMaxForce = 0.005f;
		mCDetect = 15.0f;
	}

	mPosition.x = mX;
	mPosition.y = mY;
	mPosition.z = mZ;

	// Initialize velocity of all ships to be random.
	mVelocity.x = ((float)rand() * RAND_MAX) - 0.5f;
	mVelocity.y = ((float)rand() * RAND_MAX) - 0.5f;
	mVelocity.z = ((float)rand() * RAND_MAX) - 0.5f;
	mVelocity.Normalize();
	mAccel.Multiply(0.0);

	// The orientation of the ship with no rotation is (0, 0, 1).
	mOldX = 0.0f;
	mOldY = 0.0f;
	mOldZ = 1.0f;

	// Initialize the Direct3D counts. 
	mShipVertexCount = 0;
	mShipIndexCount = 0;

	// Set up its location matirx within 3 Demensional space.
	XMMATRIX I = XMMatrixIdentity();
	XMStoreFloat4x4(&mEnemyShipWorld, I);
	XMStoreFloat4x4(&mEnemyShipTexTransform, I);

	// Material light properties for an individual ship being rendered to the screen.
	mEnemyShipMat.Ambient = XMFLOAT4(1.0f, 1.0f, 1.0f, 0.0f);
	mEnemyShipMat.Diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	mEnemyShipMat.Specular = XMFLOAT4(0.6f, 0.6f, 0.6f, 16.0f);

	// Placement in 3-Demensional space.
	XMStoreFloat4x4(&mEnemyShipWorld, XMMatrixTranslation(mX, mY, mZ));
}

// Destructor EnemyShip()
EnemyShip::~EnemyShip() {
	ReleaseCOM(mEnemyShipVB);
	ReleaseCOM(mEnemyShipIB);
	ReleaseCOM(mEnemyShipDiffuseMapSRV);
}

// Creates and stores buffers for the vertices and indices of the model. IRRELEVENT TO AI.
void EnemyShip::BuildEnemyShipGeometryBuffers(ID3D11Device* md3dDevice) {
	GeometryGenerator::MeshData ship;
	GeometryGenerator geoGen;

	geoGen.CreateCylinder(mBase, 0.002f, mHeight, 50, 50, ship);
	mShipVertexCount = ship.Vertices.size();

	std::vector<Vertex::Basic32> vertices(mShipVertexCount);

	for (size_t i = 0; i < mShipVertexCount; ++i) {
		vertices[i].Pos = ship.Vertices[i].Position;
		vertices[i].Tex = ship.Vertices[i].TexC;
		vertices[i].Normal = ship.Vertices[i].Normal;
	}

	D3D11_BUFFER_DESC vertexBufferDesc;
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.ByteWidth = sizeof(Vertex::Basic32) * mShipVertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	D3D11_SUBRESOURCE_DATA vertexInitData;
	vertexInitData.pSysMem = &vertices[0];
	HR(md3dDevice->CreateBuffer(&vertexBufferDesc, &vertexInitData, &mEnemyShipVB));

	// Pack the indices of all the meshes into one index buffer.
	std::vector<UINT> indices;
	indices.insert(indices.end(), ship.Indices.begin(), ship.Indices.end());
	mShipIndexCount = ship.Indices.size();

	D3D11_BUFFER_DESC indexBufferDesc;
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.ByteWidth = sizeof(UINT) * mShipIndexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	D3D11_SUBRESOURCE_DATA indexInitData;
	indexInitData.pSysMem = &indices[0];
	HR(md3dDevice->CreateBuffer(&indexBufferDesc, &indexInitData, &mEnemyShipIB));

	// Apply a texture to the ship. This texture is temporary. I just need one for proper light diffusion or else you wouldn't even
	// see the cones.
	HR(D3DX11CreateShaderResourceViewFromFileW(md3dDevice, L"Assets/Textures/Borg.dds", 0, 0, &mEnemyShipDiffuseMapSRV, 0));
}

/* Draw() - Draw a EnemyShip to the screen as a cone.
*  The orientation of the cone matches the velocity vector of the EnemyShip. */
void EnemyShip::Draw(ID3D11DeviceContext* device, ID3DX11EffectTechnique* activeTech, const Camera& cam) {
	UINT stride = sizeof(Vertex::Basic32);
	UINT offset = 0;
	XMMATRIX view = cam.View();
	XMMATRIX proj = cam.Proj();
	XMMATRIX viewProj = cam.ViewProj();

	// Orient the boid based on velocity vector.
	float magnitude = mVelocity.Magnitude();
	if (magnitude > 0.0) {
		float magB = sqrt(pow(mOldX, 2) + pow(mOldY, 2) + pow(mOldZ, 2));
		float xNorm = mVelocity.x / magnitude;
		float yNorm = mVelocity.y / magnitude;
		float zNorm = mVelocity.z / magnitude;

		// Translate and rotate the ship into it's new position and direction.
		XMFLOAT3 axis(1.0f, 1.0f, 1.0f);
		//glRotatef(acos(mOldX * xNorm + mOldY * yNorm + mOldZ * zNorm) * 180 / M_PI, mOldY * zNorm - mOldZ * yNorm, mOldZ * xNorm - mOldX * zNorm, mOldX * yNorm - mOldY * xNorm);
		//float angle = acos(((mOldX * mVelocity.x) + (mOldY * mVelocity.y) + (mOldZ * mVelocity.z)) / (magnitude * magB));
		float angle = acos((mOldX * xNorm) + (mOldY * yNorm) + (mOldZ * zNorm));
		XMMATRIX M = XMMatrixRotationAxis(XMLoadFloat3(&axis), angle) * XMMatrixTranslation(mPosition.x, mPosition.y, mPosition.z);
		XMStoreFloat4x4(&mEnemyShipWorld, M);
	}

	D3DX11_TECHNIQUE_DESC techDesc;
	activeTech->GetDesc(&techDesc);

	// Rendering a single ship to the screen.
	for (UINT p = 0; p < techDesc.Passes; ++p) {
		device->IASetVertexBuffers(0, 1, &mEnemyShipVB, &stride, &offset);
		device->IASetIndexBuffer(mEnemyShipIB, DXGI_FORMAT_R32_UINT, 0);

		XMMATRIX world = XMLoadFloat4x4(&mEnemyShipWorld);
		XMMATRIX worldInvTranspose = MathHelper::InverseTranspose(world);
		XMMATRIX worldViewProj = world * view * proj;

		Effects::BasicFX->SetWorld(world);
		Effects::BasicFX->SetWorldInvTranspose(worldInvTranspose);
		Effects::BasicFX->SetWorldViewProj(worldViewProj);
		Effects::BasicFX->SetTexTransform(XMLoadFloat4x4(&mEnemyShipTexTransform));
		Effects::BasicFX->SetMaterial(mEnemyShipMat);
		Effects::BasicFX->SetDiffuseMap(mEnemyShipDiffuseMapSRV);

		activeTech->GetPassByIndex(p)->Apply(0, device);
		device->DrawIndexed(mShipIndexCount, 0, 0);
	}
}

// GetDistance() - return the distance to the other EnemyShip.
float EnemyShip::GetDistance(EnemyShip* other) {
	return sqrt(pow(other->mPosition.x - this->mPosition.x, 2) + pow(other->mPosition.y - this->mPosition.y, 2) + pow(other->mPosition.z - this->mPosition.z, 2));
}

// Update() - update the internal logic of the EnemyShip.
void EnemyShip::Update(std::vector<EnemyShip*> &flock, EnemyShip* predator) {
	Vec3 avoidance(0.0, 0.0, 0.0);
	Vec3 matching(0.0, 0.0, 0.0);
	Vec3 centering(0.0, 0.0, 0.0);
	Vec3 obstacles(0.0, 0.0, 0.0);
	Vec3 avoidPredator(0.0, 0.0, 0.0);
	float aCount = 0.0;
	float mCount = 0.0;
	float cCount = 0.0;

	// Check for interflock forces.
	for (int i = 0; i < flock.size(); i++) {
		EnemyShip* b = flock.at(i);
		float distance = this->GetDistance(b);

		// COLLISION AVOIDANCE
		// Avoid collisions with nearby ships.
		if (distance < mADetect && distance > 0.00000001) {
			aCount += 1.0;
			avoidance.Add(mPosition.x - b->mPosition.x, mPosition.y - b->mPosition.y, mPosition.z - b->mPosition.z);
		}

		// VELOCITY MATCHING
		// Attempt to match velocity of nearby ships.
		if (distance < mMDetect && distance > 0.00000001) {
			mCount += 1.0;
			matching.Add(b->mVelocity);
		}

		// FLOCK CENTERING
		// Attempt to stay close to nearby ships.
		if (distance < mCDetect && distance > 0.00000001) {
			cCount += 1.0;
			centering.Add(b->mPosition);
		}
	}

	// Avoid the predator if it is around.
	if (mIsPredator == false && predator != NULL) {
		float distance = this->GetDistance(predator);
		if (distance < 30.0) {
			avoidPredator.x = mPosition.x - predator->mPosition.x;
			avoidPredator.y = mPosition.y - predator->mPosition.y;
			avoidPredator.z = mPosition.z - predator->mPosition.z;
			avoidPredator.Truncate(mMaxForce);

			// Scale for predator avoidance is 3.0
			avoidPredator.Multiply(3.0);

			// Add force
			mAccel.Add(avoidPredator);
		}
	}

	// Accelerate based on avoidance with ships.
	if (aCount > 0 && mIsPredator == false) {
		avoidance.Divide(aCount);
		avoidance.Truncate(mMaxForce);

		// Scale for avoidance is 2.0
		avoidance.Multiply(2.0);

		// Add force
		mAccel.Add(avoidance);
	}

	// Accelerate based on matching velocity with ships.
	if (mCount > 0 && mIsPredator == false) {
		matching.Divide(mCount);
		matching.Subtract(mVelocity);
		matching.Truncate(mMaxForce);

		// Scale for matching is 1.0
		matching.Multiply(1.0);

		// Add force
		mAccel.Add(matching);
	}

	// Accelerate mBased on centering towards ships
	if (cCount > 0) {
		centering.Divide(cCount);
		centering.Subtract(mPosition);
		centering.Truncate(mMaxForce);

		// Scale for centering is 1.0
		centering.Multiply(1.0);

		// Add force
		mAccel.Add(centering);
	}

	// Change velocity based on acceleration vector.
	mVelocity.Add(mAccel);

	// Don't go past maximum speed.
	mVelocity.Truncate(mMaxSpeed);

	// Don't go too far beyond the invisible box boundries.
	if (mPosition.x > MAX_X) { mVelocity.x -= 0.001; }
	if (mPosition.x < MIN_X) { mVelocity.x += 0.001; }
	if (mPosition.y > MAX_Y) { mVelocity.y -= 0.001; }
	if (mPosition.y < MIN_Y) { mVelocity.y += 0.001; }
	if (mPosition.z > MAX_Z) { mVelocity.z -= 0.001; }
	if (mPosition.z < MIN_Z) { mVelocity.z += 0.001; }

	// Change position based on velocity vector.
	mPosition.Add(mVelocity);

	// Reset accelerations to 0 after this loop.
	mAccel.Multiply(0.0);
}
