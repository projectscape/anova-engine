#pragma once
#ifndef ENEMYSHIP_H
#define ENEMYSHIP_H

#include <vector>
#include <math.h>
#include <random>
#include "d3dApp.h"
#include "Effects.h"
#include "GeometryGenerator.h"
#include "Vec3.h"
#include "Vertex.h"
#include "3DModelLoader.h"
#include <PxActor.h>

// This is roughly the dimensions of the box that the ships fly around in 
// (if you need to change the dimensions change them here).
#define MAX_X 60
#define MIN_X -60
#define	MAX_Y 60
#define MIN_Y -60
#define MAX_Z 60
#define MIN_Z -60

class Camera;

class EnemyShip {
private:
	UINT mShipHealth;
	UINT mDamageDealt;
	float mMaxSpeed;
	float mMaxForce;
	float mOldX, mOldY, mOldZ;
	Vec3 mPosition;
	Vec3 mVelocity;
	Vec3 mAccel;
	bool mIsPredator;
	float mBase;
	float mHeight;
	float mADetect;
	float mMDetect;
	float mCDetect;

	// Private varibles needed to draw in Direct3D.
	ID3D11Buffer* mEnemyShipVB;
	ID3D11Buffer* mEnemyShipIB;
	XMFLOAT4X4 mEnemyShipWorld;
	XMFLOAT4X4 mEnemyShipTexTransform;
	Material mEnemyShipMat;
	ID3D11ShaderResourceView* mEnemyShipDiffuseMapSRV;

	// The eventual model of the ship.
	ModelLoader* mShipModel;

	// Private methods.
	UINT GetHealth();
	ModelLoader* ScaleModel(ModelLoader* model, int size, float scaleFactor);
	float GetDistance(EnemyShip* other);

public:
	physx::PxActor* actor;

	EnemyShip(float mX, float mY, float mZ, bool isPredator);
	~EnemyShip();
	void Draw(ID3D11DeviceContext* device, ID3DX11EffectTechnique* activeTech, const Camera& cam);
	void Update(std::vector<EnemyShip*> &flock, EnemyShip* predator);
	void BuildEnemyShipGeometryBuffers(ID3D11Device* md3dDevice);

	// Public variables needed to draw in Direct3D.
	unsigned int mShipVertexCount;
	unsigned int mShipIndexCount;
};

#endif // ENEMYSHIP_H