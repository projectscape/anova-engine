#include "Player.h"

Player::Player(ID3D11Device* md3dDevice) : mTurnAnimationAngle(0.0f), mLastRotateAngle(0.0f), mHealth(100), mPlayerShipDiffuseMapSRV(0), mPlayerShipVB(0),
mPosition(0.0f, 0.0f, 0.0f) {
	// Initialize the Direct3D counts. 
	mPlayerShipVertexCount = 0;

	XMMATRIX I = XMMatrixIdentity();
	XMStoreFloat4x4(&mPlayerShipWorld, I);
	XMStoreFloat4x4(&mPlayerShipTexTransform, I);

	// Material light properties for the player ship being rendered to the screen.
	mPlayerShipMat.Ambient = XMFLOAT4(1.0f, 1.0f, 1.0f, 0.0f);
	mPlayerShipMat.Diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	mPlayerShipMat.Specular = XMFLOAT4(0.6f, 0.6f, 0.6f, 16.0f);

	for (int i = 0; i < 30; ++i) {
		Bullet* bullet = new Bullet();
		bullet->BuildBulletGeometryBuffers(md3dDevice);
		mBullets.push_back(bullet);
	}
}

Player::~Player() {
	ReleaseCOM(mPlayerShipVB);
	ReleaseCOM(mPlayerShipDiffuseMapSRV);
}

void Player::Draw(ID3D11DeviceContext* device, ID3DX11EffectTechnique* activeTech, const Camera& cam) {
	UINT stride = sizeof(Vertex::Basic32);
	UINT offset = 0;
	XMMATRIX view = cam.View();
	XMMATRIX proj = cam.Proj();
	XMMATRIX viewProj = cam.ViewProj();

	// The player model's origin position on the screen.
	mPosition.x = cam.GetPosition().x;
	mPosition.y = cam.GetPosition().y;
	mPosition.z = cam.GetPosition().z;

	float rotateAngle = cam.mRotateAngle;
	float pitchAngle = cam.mPitchAngle;

	// Animation calculations.
	if (mTurnAnimationAngle > MathHelper::Pi / 2) { mTurnAnimationAngle = MathHelper::Pi / 2; }
	if (mTurnAnimationAngle < -MathHelper::Pi / 2) { mTurnAnimationAngle = -MathHelper::Pi / 2; }

	// Apply the rotations and transformations.
	XMMATRIX M = XMMatrixRotationZ(-mTurnAnimationAngle) * XMMatrixRotationX(pitchAngle) * XMMatrixRotationY(rotateAngle) * XMMatrixTranslation(mPosition.x, mPosition.y, mPosition.z);
	XMStoreFloat4x4(&mPlayerShipWorld, M);

	// Record the last angle that the camera was at.
	mLastRotateAngle = rotateAngle;

	// Drawing the actual model to the screen.
	D3DX11_TECHNIQUE_DESC techDesc;
	activeTech->GetDesc(&techDesc);

	for (UINT p = 0; p < techDesc.Passes; ++p) {
		XMMATRIX world = XMLoadFloat4x4(&mPlayerShipWorld);
		XMMATRIX worldInvTranspose = MathHelper::InverseTranspose(world);
		XMMATRIX worldViewProj = world * view * proj;

		device->IASetVertexBuffers(0, 1, &mPlayerShipVB, &stride, &offset);

		Effects::BasicFX->SetWorld(world);
		Effects::BasicFX->SetWorldInvTranspose(worldInvTranspose);
		Effects::BasicFX->SetWorldViewProj(worldViewProj);
		Effects::BasicFX->SetTexTransform(XMLoadFloat4x4(&mPlayerShipTexTransform));
		Effects::BasicFX->SetMaterial(mPlayerShipMat);
		Effects::BasicFX->SetDiffuseMap(mPlayerShipDiffuseMapSRV);

		activeTech->GetPassByIndex(p)->Apply(0, device);
		device->Draw(mPlayerShipVertexCount, 0);
	}

	// If the player has fired any bullets make sure to render them to the screen.
	for (int i = 0; i < mBulletsFiredPositions.size(); ++i) {
		mBullets[i]->Draw(device, activeTech, cam, mBulletsFiredPositions[i]);
	}
}

void Player::BuildPlayerGeometryBuffers(ID3D11Device* md3dDevice) {
	ModelLoader* ship = new ModelLoader();
	ship->LoadFile("Assets/Models/FreeShuttle.obj");

	ScaleModel(ship, ship->vertexCount, 1.2f);

	std::vector<Vertex::Basic32> vertices(ship->vertexCount);

	for (size_t i = 0; i < ship->vertexCount; ++i) {
		vertices[i].Pos = ship->vertex[i].Pos;
		vertices[i].Tex = ship->vertex[i].Tex;
		vertices[i].Normal = ship->vertex[i].Normal;
	}

	mPlayerShipVertexCount = ship->vertexCount;

	D3D11_BUFFER_DESC vertexBufferDesc;
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.ByteWidth = sizeof(Vertex::Basic32) * mPlayerShipVertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	D3D11_SUBRESOURCE_DATA vertexInitData;
	vertexInitData.pSysMem = &vertices[0];
	HR(md3dDevice->CreateBuffer(&vertexBufferDesc, &vertexInitData, &mPlayerShipVB));

	// Apply a texture to the ship. This texture is temporary. I just need one for proper light diffusion or else you wouldn't even
	// see the cones.
	HR(D3DX11CreateShaderResourceViewFromFileW(md3dDevice, L"Assets/Textures/Borg.dds", 0, 0, &mPlayerShipDiffuseMapSRV, 0));
}

void Player::Fire(const Camera& cam) {
	if (mBulletsFiredPositions.size() < 30) {
		mBulletsFiredPositions.push_back(cam.GetPosition());
	}

	//Sleep(1000);
}

void Player::ScaleModel(ModelLoader* model, int size, float scaleFactor) {
	for (int i = 0; i < size; ++i) {
		model->vertex[i].Pos.x *= scaleFactor;
		model->vertex[i].Pos.y *= scaleFactor;
		model->vertex[i].Pos.z *= scaleFactor;
	}
}