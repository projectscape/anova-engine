#pragma once
#ifndef XBOXCONTROLLER_H
#define XBOXCONTROLLER_H

#include "windows.h"
#include "xinput.h"
#include "Camera.h"
#include "Player.h"

static class XboxController {
public:
	XboxController();
	~XboxController();

	static void CheckButtonPress(XINPUT_STATE state, const Camera& cam, Player* player);
	static void XboxController::CheckJoystickMovement(XINPUT_STATE state, Camera* cam, Player* player, float speed, float dt);
};

#endif // XBOXCONTROLLER_H