#pragma once
#ifndef PLAYER_H
#define PLAYER_H

#include "Bullet.h"
#include "Camera.h"
#include "d3dApp.h"
#include "Effects.h"
#include "GeometryGenerator.h"
#include "Vertex.h"
#include "3DModelLoader.h"
#define UINT unsigned int

class Player {
public:
	Player(ID3D11Device* md3dDevice);
	~Player();
	void Draw(ID3D11DeviceContext* device, ID3DX11EffectTechnique* activeTech, const Camera& cam);
	void BuildPlayerGeometryBuffers(ID3D11Device* md3dDevice);
	void Fire(const Camera& cam);

	float mTurnAnimationAngle;
	std::vector<Bullet*> mBullets;
	std::vector<XMFLOAT3> mBulletsFiredPositions;

private:
	float mLastRotateAngle;
	float mLastPitchAngle;
	UINT mHealth;
	XMFLOAT3 mPosition;

	ID3D11Buffer* mPlayerShipVB;
	UINT mPlayerShipVertexCount;
	XMFLOAT4X4 mPlayerShipWorld;
	XMFLOAT4X4 mPlayerShipTexTransform;
	Material mPlayerShipMat;
	ID3D11ShaderResourceView* mPlayerShipDiffuseMapSRV;

	void ScaleModel(ModelLoader* model, int size, float scaleFactor);
};

#endif //PLAYER_H