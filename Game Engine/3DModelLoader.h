#pragma once
#ifndef _3DMODELLOADER_H_
#define _3DMODELLOADER_H_

#include "xnamath.h"
#include <vector>
#include <fstream>
#include "Vertex.h"

class ModelLoader {
	public:
		ModelLoader();
		~ModelLoader();

	public:
		bool LoadFile(char* szFileName);
		void Clear();

	public:
		Vertex::Basic32* vertex;
		unsigned long vertexCount;
};
#endif // _3DMODELLOADER_H_
