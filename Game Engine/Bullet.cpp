#include "Bullet.h"

Bullet::Bullet() {
	XMMATRIX I = XMMatrixIdentity();
	XMStoreFloat4x4(&mBulletWorld, I);
	XMStoreFloat4x4(&mBulletTexTransform, I);

	// Material light properties for the player ship being rendered to the screen.
	mBulletMat.Ambient = XMFLOAT4(1.0f, 1.0f, 1.0f, 0.0f);
	mBulletMat.Diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	mBulletMat.Specular = XMFLOAT4(0.6f, 0.6f, 0.6f, 16.0f);
}

Bullet::~Bullet() {
	ReleaseCOM(mBulletVB);
	ReleaseCOM(mBulletIB);
	ReleaseCOM(mBulletDiffuseMapSRV);
}

void Bullet::Draw(ID3D11DeviceContext* device, ID3DX11EffectTechnique* activeTech, const Camera& cam, XMFLOAT3 position) {
	UINT stride = sizeof(Vertex::Basic32);
	UINT offset = 0;
	XMMATRIX view = cam.View();
	XMMATRIX proj = cam.Proj();
	XMMATRIX viewProj = cam.ViewProj();

	XMMATRIX M = XMMatrixTranslation(position.x, position.y, position.z);
	XMStoreFloat4x4(&mBulletWorld, M);

	D3DX11_TECHNIQUE_DESC techDesc;
	activeTech->GetDesc(&techDesc);

	// Rendering a single ship to the screen.
	for (UINT p = 0; p < techDesc.Passes; ++p) {
		device->IASetVertexBuffers(0, 1, &mBulletVB, &stride, &offset);
		device->IASetIndexBuffer(mBulletIB, DXGI_FORMAT_R32_UINT, 0);

		XMMATRIX world = XMLoadFloat4x4(&mBulletWorld);
		XMMATRIX worldInvTranspose = MathHelper::InverseTranspose(world);
		XMMATRIX worldViewProj = world * view * proj;

		Effects::BasicFX->SetWorld(world);
		Effects::BasicFX->SetWorldInvTranspose(worldInvTranspose);
		Effects::BasicFX->SetWorldViewProj(worldViewProj);
		Effects::BasicFX->SetTexTransform(XMLoadFloat4x4(&mBulletTexTransform));
		Effects::BasicFX->SetMaterial(mBulletMat);
		Effects::BasicFX->SetDiffuseMap(mBulletDiffuseMapSRV);

		activeTech->GetPassByIndex(p)->Apply(0, device);
		device->DrawIndexed(mBulletIndexCount, 0, 0);
	}
}

void Bullet::CalculateDirectionVector(const Camera& cam) {

}

void Bullet::BuildBulletGeometryBuffers(ID3D11Device* device) {
	GeometryGenerator::MeshData bullet;
	GeometryGenerator geoGen;

	geoGen.CreateSphere(0.5f, 20, 20, bullet);
	mBulletVertexCount = bullet.Vertices.size();

	std::vector<Vertex::Basic32> vertices(mBulletVertexCount);
	for (int i = 0; i < mBulletVertexCount; ++i) {
		vertices[i].Pos = bullet.Vertices[i].Position;
		vertices[i].Tex = bullet.Vertices[i].TexC;
		vertices[i].Normal = bullet.Vertices[i].Normal;
	}

	D3D11_BUFFER_DESC vertexBufferDesc;
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.ByteWidth = sizeof(Vertex::Basic32) * mBulletVertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	D3D11_SUBRESOURCE_DATA vertexInitData;
	vertexInitData.pSysMem = &vertices[0];
	HR(device->CreateBuffer(&vertexBufferDesc, &vertexInitData, &mBulletVB));

	// Pack the indices of all the meshes into one index buffer.
	std::vector<UINT> indices;
	indices.insert(indices.end(), bullet.Indices.begin(), bullet.Indices.end());
	mBulletIndexCount = bullet.Indices.size();

	D3D11_BUFFER_DESC indexBufferDesc;
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.ByteWidth = sizeof(UINT) * mBulletIndexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	D3D11_SUBRESOURCE_DATA indexInitData;
	indexInitData.pSysMem = &indices[0];
	HR(device->CreateBuffer(&indexBufferDesc, &indexInitData, &mBulletIB));

	// Apply a texture to the ship. This texture is temporary. I just need one for proper light diffusion or else you wouldn't even
	// see the cones.
	HR(D3DX11CreateShaderResourceViewFromFileW(device, L"Assets/Textures/Borg.dds", 0, 0, &mBulletDiffuseMapSRV, 0));
}