#include "Vec3.h"

// x,y,z initialized to 0.
Vec3::Vec3() {
	x = 0.0;
	y = 0.0;
	z = 0.0;
}

// x, y, z initialized to passed values.
Vec3::Vec3(double mX, double mY, double mZ) {
	x = mX;
	y = mY;
	z = mZ;
}

// Scale by the given factor.
void Vec3::Multiply(double scalar) {
	x *= scalar;
	y *= scalar;
	z *= scalar;
}

// Divide each component by scalar.
void Vec3::Divide(double scalar) {
	x /= scalar;
	y /= scalar;
	z /= scalar;
}

// Return the norm length of vector.
double Vec3::Magnitude() {
	return sqrt(x * x + y * y + z * z);
}

// Limit the magnitude of this vector to be scalar
void Vec3::Truncate(double scalar) {
	float myMag = this->Magnitude();
	if (myMag > scalar) {
		x *= scalar / myMag;
		y *= scalar / myMag;
		z *= scalar / myMag;
	}
}

// Set the magnitude to be 1.0.
void Vec3::Normalize() {
	x /= this->Magnitude();
	y /= this->Magnitude();
	z /= this->Magnitude();
}

// Add x,y,z componentwise.
void Vec3::Add(double oX, double oY, double oZ) {
	x += oX;
	y += oY;
	z += oZ;
}

// Add another vector.
void Vec3::Add(Vec3 other) {
	x += other.x;
	y += other.y;
	z += other.z;
}

// Subtract another vector from this one.
void Vec3::Subtract(Vec3 other) {
	x -= other.x;
	y -= other.y;
	z -= other.z;
}
