#pragma once
#ifndef BULLET_H
#define BULLET_H

#include "d3dApp.h"
#include "Camera.h"
#include "Effects.h"
#include "GeometryGenerator.h"
#include "Vertex.h"
#define UINT unsigned int

class Player;

class Bullet {
	public:
		Bullet();
		~Bullet();
		void Draw(ID3D11DeviceContext* device, ID3DX11EffectTechnique* activeTech, const Camera& cam, XMFLOAT3 position);
		void BuildBulletGeometryBuffers(ID3D11Device* md3dDevice);
		void CalculateDirectionVector(const Camera& cam);

	private:
		XMFLOAT3 mPosition;

		ID3D11Buffer* mBulletVB;
		ID3D11Buffer* mBulletIB;
		UINT mBulletVertexCount;
		UINT mBulletIndexCount;
		XMFLOAT4X4 mBulletWorld;
		XMFLOAT4X4 mBulletTexTransform;
		Material mBulletMat;
		ID3D11ShaderResourceView* mBulletDiffuseMapSRV;
};

#endif //BULLET_H