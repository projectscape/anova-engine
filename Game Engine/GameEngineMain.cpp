//***************************************************************************************
// ANOVA Engine
// by Daniel Diaz (C) 2014 All Rights Reserved.
//***************************************************************************************

#include <time.h>
#include "Camera.h"
#include "d3dApp.h"
#include "d3dx11Effect.h"
#include "Effects.h"
#include "EnemyShip.h"
#include "GeometryGenerator.h"
#include "LightHelper.h"
#include "MathHelper.h"
#include "3DModelLoader.h"
#include "Sky.h"
#include "Vertex.h"
#include "XboxController.h"

#define NUM_OF_ENEMY_SHIPS 100

class GameEngine : public D3DApp {
	public:
		GameEngine(HINSTANCE hInstance);
		~GameEngine();

		bool Init();
		void OnResize();
		void UpdateScene(float dt);
		void DrawScene();

		// These methods will only be called if the Xbox 360 controller is not connected.
		void OnMouseDown(WPARAM btnState, int x, int y);
		void OnMouseUp(WPARAM btnState, int x, int y);
		void OnMouseMove(WPARAM btnState, int x, int y);

	private:
		void BuildShipGeometryBuffers();
		ModelLoader* ScaleModel(ModelLoader* model, int size, float scaleFactor);

	private:
		ID3D11Buffer* shipVB;

		ID3D11ShaderResourceView* shipDiffuseMapSRV;

		DirectionalLight dirLights[3];
		Material shipMat;
		Sky* sky;

		XMFLOAT4X4 shipTexTransform;
		XMFLOAT4X4 shipWorld;

		XMFLOAT4X4 view;
		XMFLOAT4X4 proj;

		UINT shipVertexCount;

		XMFLOAT3 eyePosW;

		float theta, phi, radius, cameraSpeed;

		ID3D11RasterizerState* rasterizerState;
		Camera camera;
		XINPUT_STATE controllerState;
		bool controllerConnected;
		POINT lastMousePos;
		std::vector<EnemyShip*> ships;
		Player* player;
		EnemyShip* predator;
};

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, PSTR cmdLine, int showCmd) {
	// Enable run-time memory check for debug builds.
	#if defined(DEBUG) | defined(_DEBUG)
		_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	#endif

	GameEngine game(hInstance);

	if (!game.Init())
		return 0;

	return game.Run();
}


GameEngine::GameEngine(HINSTANCE hInstance) : D3DApp(hInstance), shipVB(0), shipDiffuseMapSRV(0), shipVertexCount(0), eyePosW(0.0f, 0.0f, 0.0f), theta(1.3f * MathHelper::Pi), phi(0.4f * MathHelper::Pi), 
											  radius(2.5f), cameraSpeed(25.0f), controllerConnected(false) {
	mMainWndCaption = L"The Argonaut Nine";

	lastMousePos.x = 0;
	lastMousePos.y = 0;

	camera.SetPosition(0.0f, 0.0f, -100.0f);

	// Create a world matrix out of the following matrices.
	XMMATRIX I = XMMatrixIdentity();
	XMStoreFloat4x4(&shipWorld, I);
	XMStoreFloat4x4(&shipTexTransform, I);
	XMStoreFloat4x4(&view, I);
	XMStoreFloat4x4(&proj, I);

	// Direction the light comes from and the differnt intensities of Ambient, Diffuse, and Specular lighting.
	dirLights[0].Ambient = XMFLOAT4(0.3f, 0.3f, 0.3f, 1.0f);
	dirLights[0].Diffuse = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	dirLights[0].Specular = XMFLOAT4(0.6f, 0.6f, 0.6f, 16.0f);
	dirLights[0].Direction = XMFLOAT3(0.707f, 0.0f, -0.707f);

	dirLights[1].Ambient = XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f);
	dirLights[1].Diffuse = XMFLOAT4(1.4f, 1.4f, 1.4f, 1.0f);
	dirLights[1].Specular = XMFLOAT4(0.3f, 0.3f, 0.3f, 16.0f);
	dirLights[1].Direction = XMFLOAT3(0.707f, 0.0f, -0.707f);

	// Material light properties for all of the objects being rendered to the screen.
	shipMat.Ambient = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	shipMat.Diffuse = XMFLOAT4(0.9f, 0.9f, 0.9f, 1.0f);
	shipMat.Specular = XMFLOAT4(0.6f, 0.6f, 0.6f, 16.0f);
}

GameEngine::~GameEngine() {
	ReleaseCOM(shipVB);
	ReleaseCOM(shipDiffuseMapSRV);
	SafeDelete(sky);

	Effects::DestroyAll();
	InputLayouts::DestroyAll();
}

bool GameEngine::Init() {
	if (!D3DApp::Init())
		return false;

	// Must init Effects first since InputLayouts depend on shader signatures.
	// Also handle all initialization here.
	Effects::InitAll(md3dDevice);
	InputLayouts::InitAll(md3dDevice);

	// Place the complex 3D model into postion.
	XMMATRIX M = XMMatrixTranslation(100.0, -60.0, 0.0);
	XMStoreFloat4x4(&shipWorld, M);

	sky = new Sky(md3dDevice, L"Assets/Textures/SunInSpace.dds", 6000.0f);
	HR(D3DX11CreateShaderResourceViewFromFileW(md3dDevice, L"Assets/Textures/borg.dds", 0, 0, &shipDiffuseMapSRV, 0));

	BuildShipGeometryBuffers();

	// Populate the EnemyShip vector with maximum number of enemy ships. Give them random starting locations and initialze their 
	// respective Geometry Buffers.
	for (unsigned int i = 0; i < NUM_OF_ENEMY_SHIPS; ++i) {
		float x = rand() % (MAX_X * 2);
		x -= MAX_X;
		float y = rand() % (MAX_Y * 2);
		y -= MAX_Y;
		float z = rand() % (MAX_Z * 2);
		z -= MAX_Z;

		// Populates the ships in a random postion within the designated space.
		ships.push_back(new EnemyShip(x, y, z, false));
		ships[i]->BuildEnemyShipGeometryBuffers(md3dDevice);
	}

	predator = nullptr;

	// Build the geometry buffers of the player.
	player = new Player(md3dDevice);
	player->BuildPlayerGeometryBuffers(md3dDevice);

	mSwapChain->SetFullscreenState(true, nullptr);
	ShowCursor(false);

	// Set the state of the Rasterizer.
	D3D11_RASTERIZER_DESC rast;
	ZeroMemory(&rast, sizeof(D3D11_RASTERIZER_DESC));
	rast.FillMode = D3D11_FILL_SOLID;
	rast.CullMode = D3D11_CULL_BACK;
	rast.FrontCounterClockwise = false;
	rast.DepthClipEnable = true;
	HR(md3dDevice->CreateRasterizerState(&rast, &rasterizerState));

	return true;
}

void GameEngine::OnResize() {
	D3DApp::OnResize();
	camera.SetLens(0.25f * MathHelper::Pi, AspectRatio(), 1.0f, 3000.0f);
}

void GameEngine::UpdateScene(float dt) {
	// Check to make sure that the Xbox 360 controller is still connected.
	if (XInputGetState(0, &controllerState) == ERROR_SUCCESS) {
		controllerConnected = true;

		// Check to see if any of the Xbox buttons were pressed.
		XboxController::CheckButtonPress(controllerState, camera, player);

		// Update the state of both the left and the right joysticks.
		XboxController::CheckJoystickMovement(controllerState, &camera, player, cameraSpeed, dt);
	}

	// Otherwise the controller is not connected and we must use the keyboard.
	else {
		controllerConnected = false;

		if (GetAsyncKeyState('W') & 0x80000)
			camera.Walk(cameraSpeed * dt);

		if (GetAsyncKeyState('S') & 0x8000)
			camera.Walk(-cameraSpeed * dt);

		if (GetAsyncKeyState('A') & 0x8000)
			camera.Strafe(-cameraSpeed * dt);

		if (GetAsyncKeyState('D') & 0x8000)
			camera.Strafe(cameraSpeed * dt);
	}
}

void GameEngine::OnMouseDown(WPARAM btnState, int x, int y) {
	if (!controllerConnected) {
		//player->Fire(mCamera);
	}
}

void GameEngine::OnMouseUp(WPARAM btnState, int x, int y) {
	if (!controllerConnected)
		ReleaseCapture();
}

void GameEngine::OnMouseMove(WPARAM btnState, int x, int y) {
	if (!controllerConnected) {
		if ((btnState & MK_LBUTTON) != 0) {
			//Make each pixel correspond to a quarter of a degree.
			float dx = XMConvertToRadians(0.25f * static_cast<float>(x - lastMousePos.x));
			float dy = XMConvertToRadians(0.25f * static_cast<float>(y - lastMousePos.y));

			camera.Pitch(dy);
			camera.RotateY(dx);
		}

		lastMousePos.x = x;
		lastMousePos.y = y;
	}
}

void GameEngine::DrawScene() {
	md3dImmediateContext->ClearRenderTargetView(mRenderTargetView, reinterpret_cast<const float*>(&Colors::Black));
	md3dImmediateContext->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	md3dImmediateContext->IASetInputLayout(InputLayouts::Basic32);
	md3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	md3dImmediateContext->RSSetState(rasterizerState);

	camera.UpdateViewMatrix();
	XMMATRIX view = camera.View();
	XMMATRIX proj = camera.Proj();
	XMMATRIX viewProj = camera.ViewProj();

	UINT stride = sizeof(Vertex::Basic32);
	UINT offset = 0;

	// Set per frame constants.
	Effects::BasicFX->SetDirLights(dirLights);
	Effects::BasicFX->SetEyePosW(eyePosW);

	ID3DX11EffectTechnique* activeTech = Effects::BasicFX->Light2TexTech;

	D3DX11_TECHNIQUE_DESC techDesc;
	activeTech->GetDesc(&techDesc);

	// Draw The Argonaut.
	for (UINT p = 0; p < techDesc.Passes; ++p) {
		XMMATRIX world = XMLoadFloat4x4(&shipWorld);
		XMMATRIX worldInvTranspose = MathHelper::InverseTranspose(world);
		XMMATRIX worldViewProj = world * view * proj;

		md3dImmediateContext->IASetVertexBuffers(0, 1, &shipVB, &stride, &offset);

		Effects::BasicFX->SetWorld(world);
		Effects::BasicFX->SetWorldInvTranspose(worldInvTranspose);
		Effects::BasicFX->SetWorldViewProj(worldViewProj);
		Effects::BasicFX->SetTexTransform(XMLoadFloat4x4(&shipTexTransform));
		Effects::BasicFX->SetMaterial(shipMat);
		Effects::BasicFX->SetDiffuseMap(shipDiffuseMapSRV);

		activeTech->GetPassByIndex(p)->Apply(0, md3dImmediateContext);
		md3dImmediateContext->Draw(shipVertexCount, 0);
	}

	// Draw the enemy ships in their updated positions.
	for (UINT i = 0; i < NUM_OF_ENEMY_SHIPS; ++i) {
		ships[i]->Update(ships, predator);
		ships[i]->Draw(md3dImmediateContext, activeTech, camera);
	}

	// Draw the player model in it's new position.
	player->Draw(md3dImmediateContext, activeTech, camera);

	// Draw the cube map.
	sky->Draw(md3dImmediateContext, camera);

	// Swap the rendering buffers.
	HR(mSwapChain->Present(0, 0));
}

void GameEngine::BuildShipGeometryBuffers() {
	/*ComplexModel* ship = new ComplexModel();
	ship->LoadFile("Assets/Models/Perun_BS_2.obj");*/

	ModelLoader* ship = new ModelLoader();
	ship->LoadFile("Assets/Models/Perun_BS_2.obj");

	std::vector<Vertex::Basic32> vertices(ship->vertexCount);

	for (size_t i = 0; i < ship->vertexCount; ++i) {
		vertices[i].Pos = ship->vertex[i].Pos;
		vertices[i].Tex = ship->vertex[i].Tex;
		vertices[i].Normal = ship->vertex[i].Normal;
	}

	shipVertexCount = ship->vertexCount;

	D3D11_BUFFER_DESC vertexBufferDesc;
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.ByteWidth = sizeof(Vertex::Basic32) * shipVertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	D3D11_SUBRESOURCE_DATA vertexInitData;
	vertexInitData.pSysMem = &vertices[0];
	HR(md3dDevice->CreateBuffer(&vertexBufferDesc, &vertexInitData, &shipVB));
}

ModelLoader* GameEngine::ScaleModel(ModelLoader* model, int size, float scaleFactor) {
	// Size the model.
	for (size_t i = 0; i < size; ++i) {
		model->vertex[i].Pos.x *= scaleFactor;
		model->vertex[i].Pos.y *= scaleFactor;
		model->vertex[i].Pos.z *= scaleFactor;
	}

	return model;
}