#include "XboxController.h"

XboxController::XboxController() {

}

XboxController::~XboxController() {

}

void XboxController::CheckButtonPress(XINPUT_STATE state, const Camera& cam, Player* player) {
	if (state.Gamepad.bRightTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD) {
		player->Fire(cam);
	}
}

void XboxController::CheckJoystickMovement(XINPUT_STATE state, Camera* cam, Player* player, float speed, float dt) {
	float LX = state.Gamepad.sThumbLX;
	float LY = state.Gamepad.sThumbLY;

	// Determine how far the controller is pushed.
	float lMagnitude = sqrt((LX * LX) + (LY * LY));

	// Determine the direction the controller is pushed.
	float normalizedLX = LX / lMagnitude;
	float normalizedLY = LY / lMagnitude;

	float lNormalizedMagnitude = 0;

	// Check if the controller is outside a circular dead zone.
	if (lMagnitude > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) {
		// Clip the magnitude at its expected maximum value.
		if (lMagnitude > 32767) { lMagnitude = 32767; }

		// Adjust magnitude relative to the end of the dead zone.
		lMagnitude -= XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE;

		// Optionally normalize the magnitude with respect to its expected range.
		// Giving a magnitude value of 0.0 to 1.0.
		lNormalizedMagnitude = lMagnitude / (32767 - XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);

		// Do movement of the player.
		if (state.Gamepad.sThumbLY > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE && state.Gamepad.sThumbLX < 25000.0f && state.Gamepad.sThumbLX > -25000.0f) {
			cam->Walk(speed * dt);
		}

		else if (state.Gamepad.sThumbLY < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE && state.Gamepad.sThumbLX < 25000.0f && state.Gamepad.sThumbLX > -25000.0f) {
			cam->Walk(speed * dt * 0.35f);
		}

		else { cam->Walk(10 * dt); }
	}

	// If the controller is in the deadzone zero out the magnitude.
	else {
		lMagnitude = 0.0;
		lNormalizedMagnitude = 0.0;
		cam->Walk(10 * dt);
	}

	// Repeat for right thumb stick.
	float RX = state.Gamepad.sThumbRX;
	float RY = state.Gamepad.sThumbRY;

	// Determine how far the controller is pushed.
	float rMagnitude = sqrt((RX * RX) + (RY * RY));

	// Determine the direction the controller is pushed.
	float normalizedRX = RX / rMagnitude;
	float normalizedRY = -RY / rMagnitude;

	float rNormalizedMagnitude = 0;

	// Check if the controller is outside a circular dead zone.
	if (rMagnitude > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE) {
		// Clip the magnitude at its expected maximum value.
		if (rMagnitude > 32767) rMagnitude = 32767;

		// Adjust magnitude relative to the end of the dead zone.
		rMagnitude -= XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE;

		// Optionally normalize the magnitude with respect to its expected range.
		// Giving a magnitude value of 0.0 to 1.0.
		rNormalizedMagnitude = rMagnitude / (32767 - XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);

		cam->Pitch(normalizedRY * rNormalizedMagnitude * 0.0108f);
		cam->RotateY(normalizedRX * rNormalizedMagnitude * 0.0108f);

		player->mTurnAnimationAngle += normalizedRX * rNormalizedMagnitude * 0.0508f;
		//player->mPitchAnimationAngle += normalizedRY * rNormalizedMagnitude * 0.0508f;
	}

	// If the controller is in the deadzone zero out the magnitude.
	else {
		rMagnitude = 0.0;
		rNormalizedMagnitude = 0.0;

		if (player->mTurnAnimationAngle > 0.0f) { player->mTurnAnimationAngle -= 0.010745329f; }
		if (player->mTurnAnimationAngle < 0.0f) { player->mTurnAnimationAngle += 0.010745329f; }
	}
}