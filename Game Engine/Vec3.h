#pragma once
#ifndef VEC3_H
#define VEC3_H

#include <math.h>

class Vec3 {
public:
	Vec3();
	Vec3(double x, double y, double z);
	void Multiply(double scalar);
	void Divide(double scalar);
	void Truncate(double scalar);
	void Add(Vec3 other);
	void Add(double x, double y, double z);
	void Subtract(Vec3 other);
	double Magnitude();
	void Normalize();

	double x, y, z;
};
#endif // VEC3_H